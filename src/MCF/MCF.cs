﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace MeinTest
{
    internal class MCF
    {
        public static void QueryMCFModList(Arguments commandLine)
        {
            // Give me all that precious data!
            Logger.WriteStatusStart("Querying MCF Mod List for JSON");
            List<MCFMods> mcfData;

            // Attempt to query JSON data from Not Enough Mods.
            try
            {
                mcfData = JSON.DownloadJSONData<List<MCFMods>>(commandLine["mcf"]);
            }
            catch (Exception)
            {
                Logger.WriteStatusFinal(false);
                return;
            }

            // Did something terrible happen?
            if (mcfData == null)
            {
                Logger.WriteStatusFinal(false);
                return;
            }

            // No errors if we got this far!
            Logger.WriteStatusFinal(true);

            // Create the internal mod list if it has not already been made.
            if (Program.ModDatas == null)
            {
                Program.ModDatas = new Dictionary<string, ModData>();
            }

            // Now iterate through the mods and create folders based on their names.
            int totalMods = 0;
            foreach (MCFMods item in mcfData)
            {
                totalMods++;
                if (totalMods < mcfData.Count)
                {
                    // If the name is longer than 100 characters just don't even bother.
                    if (item.name.ToCharArray().Length < 45)
                    {
                        Console.Write("\r" + "#" + totalMods + " - {0}                           ", item.name);
                    }
                }
                else
                {
                    Console.Write("\r" + "                                                   ");
                    Logger.WriteLine("\r" + "MCF: Deserialized " + totalMods + " mods!");
                }

                // Only count Forge mods, we don't care about you FlowerChild.
                if (!item.dependencies.Contains("Forge Required"))
                {
                    continue;
                }

                // Do not count Forge itself since we cannot load it as a mod.
                if (item.name.ToLower().Trim() == "forge" &&
                    item.other.ToLower().Trim() == "(api)")
                {
                    continue;
                }

                // If the name contains any slashes in it.
                if (item.name.Contains(Path.DirectorySeparatorChar.ToString(CultureInfo.InvariantCulture)) ||
                    item.name.Contains(Path.AltDirectorySeparatorChar.ToString(CultureInfo.InvariantCulture)))
                {
                    continue;
                }

                // If name contains any special characters like parenthesiies then we don't want it either.
                if (item.name.Contains("(") || item.name.Contains(")") || item.name.Contains("&"))
                {
                    continue;
                }

                // Correct the name of the mod to prevent naming conflicts.
                item.name =
                    item.name.Trim(Path.GetInvalidFileNameChars())
                        .Trim(Path.GetInvalidPathChars())
                        .Replace(":", string.Empty)
                        .Replace(" ", string.Empty)
                        .Replace("'", string.Empty)
                        .Replace("`", string.Empty)
                        .Replace(",", string.Empty)
                        .Trim();
                item.name = Util.RemoveSpecialCharacters(item.name);
                string dirPath = Path.Combine(Program.ModsPath, item.name);
                item.name = item.name;

                // Match up to a name already in the database, if it has one.
                bool nameExists = Program.ModDatas.Any(modData => modData.Value.Name == item.name);

                //// Create mod directory if it does not exist.
                //if (!Directory.Exists(dirPath) && !nameExists)
                //{
                //    Directory.CreateDirectory(dirPath);

                //    // Save link to mod site in the folder for easy access.
                //    string urlPath = dirPath + Path.DirectorySeparatorChar + item.name + ".url";
                //    using (var writer = new StreamWriter(urlPath))
                //    {
                //        writer.WriteLine("[InternetShortcut]");
                //        writer.WriteLine("URL=" + item.link);
                //        writer.Flush();
                //    }

                //    // Add mod to internal list for processing.
                //    var newMod = new ModData
                //    {
                //        Authors = String.Join(",", item.author.ToArray()),
                //        Name = item.name,
                //        Description = item.desc,
                //        URL = item.link,
                //        Supports = String.Join(",", item.versions.ToArray()),
                //    };

                //    // Add the mod fresh since it was not already in the list.
                //    Program.ModDatas.Add(item.name, newMod);
                //}

                // Mod already existed in list from previous JSON parse so we will add to existing dataset.
                if (Directory.Exists(dirPath) && nameExists)
                {
                    // Check if we can add author info.
                    if (string.IsNullOrEmpty(Program.ModDatas[item.name].Authors))
                    {
                        Program.ModDatas[item.name].Authors = String.Join(",", item.author.ToArray());
                    }

                    // Only add the URL if it is not already there.
                    if (string.IsNullOrEmpty(Program.ModDatas[item.name].URL))
                    {
                        Program.ModDatas[item.name].URL = item.link;
                    }

                    // Add the link to the mods source code if it exists.
                    if (!string.IsNullOrEmpty(item.source))
                    {
                        Program.ModDatas[item.name].SourceCodeLink = item.link;
                    }

                    // Fill out the nice description data that MCF gives us.
                    Program.ModDatas[item.name].Description = item.desc;

                    // Fill out the Minecraft version support data since MCF gives us that too.
                    Program.ModDatas[item.name].Supports = String.Join(",", item.versions.ToArray());
                }
            }
        }
    }
}