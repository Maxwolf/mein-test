﻿using System.Collections.Generic;

namespace MeinTest
{
    public class MCFMods
    {
        public string name { get; set; }
        public string link { get; set; }
        public string desc { get; set; }
        public List<string> author { get; set; }
        public List<string> type { get; set; }
        public List<string> dependencies { get; set; }
        public List<string> versions { get; set; }
        public string source { get; set; }
        public string other { get; set; }
    }
}