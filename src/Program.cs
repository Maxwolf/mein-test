﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Xml.Serialization;

namespace MeinTest
{
    internal class Program
    {
        // Arguments that we want to pass to Minecraft Forge on startup.
        private static string javaArguments;

        // Amount of time that we should wait before killing Minecraft Forge process.
        // Note: 1,000 ticks is 1 second (500k = 8.3 minutes).
        // PS: GregTech is stupid for having such a long startup time.
        private const int timeout = 500000;

        // Minecraft Forge running directory.

        // Control mods path which are tested against all other mods.
        private static string _ctrlPath;

        // Valid extensions that we will search for in mod folders.
        private static readonly string[] _modExtensions = {"jar", "zip"};

        // Determines how the empty mods folder should start from bottom or top of the list.
        private static bool _startFromTop = true;

        // Determines how many items we should process int he list before stopping the program and outputting results.
        private static int _stopProcessingAt = -1;

        // Stores a running amount of URL's that have been launched during testing mode to prevent same ones loading twice.
        private static readonly List<string> _launchedURLs = new List<string>();
        private static bool _shouldLoadWebsiteURL;
        public static ModData CtrlModDatas { get; set; }
        public static string ForgePath { get; set; }

        // Path to Minecraft Forge mods directory.
        public static string ForgeModPath { get; set; }

        // Mods to test for compatibility directory path.
        public static string ModsPath { get; set; }
        public static Dictionary<string, ModData> ModDatas { get; set; }

        // Timestamp for when this program was started.
        public static string StartTimestamp { get; set; }

        // Startup path for this application.
        public static string StartupPath { get; set; }

        // Determines if we should store results.
        public static bool RunningInJenkins = false;

        // Path to JDK.
        private static string javaExecutable;

        [STAThread]
        private static void Main(string[] args)
        {
            // Command line parsing
            var CommandLine = new Arguments(args);

            // Determine if we should log anything.
            Logger.WriteStatusStart("Running In Jenkins");
            if (CommandLine["jenkins"] != null)
            {
                RunningInJenkins = true;
                Logger.WriteStatusFinal(true);
            }
            else
            {
                RunningInJenkins = false;
                Logger.WriteStatusFinal(false, "NOPE", ConsoleColor.Yellow);
            }

            // Location of the Java runtime.
            Logger.WriteStatusStart("Checking JDK");
            if (CommandLine["jdk"] != null)
            {
                javaExecutable = CommandLine["jdk"];
                Logger.WriteStatusFinal(true);
            }
            else
            {
                Logger.WriteStatusFinal(false);
                Environment.Exit(-1);
            }

            // Checking Java arguments
            Logger.WriteStatusStart("Checking JDK Arguments");
            if (CommandLine["args"] != null)
            {
                javaArguments = CommandLine["args"];
                Logger.WriteStatusFinal(true);
            }
            else
            {
                Logger.WriteStatusFinal(false);
                Environment.Exit(-1);
            }

            // Determine startup path.
            StartupPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            StartTimestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            Logger.WriteLine("[PROGRAM START]");
            Logger.WriteStatusStart(Debugger.IsAttached ? "Starting in Debug" : "Starting");
            Logger.WriteStatusFinal(!String.IsNullOrEmpty(StartupPath));
            if (StartupPath == null) return;

            // Control Mod Name
            Logger.WriteStatusStart("Control Mod Name");
            if (CommandLine["name"] != null)
            {
                Logger.WriteStatusFinal(true);
            }
            else
            {
                Logger.WriteStatusFinal(false);
                Console.ReadLine();
                return;
            }

            // Control Mod Version
            Logger.WriteStatusStart("Control Mod Version");
            if (CommandLine["version"] != null)
            {
                CtrlModDatas = new ModData(CommandLine["name"], CommandLine["version"]);
                Logger.WriteStatusFinal(true);
            }
            else
            {
                Logger.WriteStatusFinal(false);
                Console.ReadLine();
                return;
            }

            // Check if we should be limiting output amount.
            Logger.WriteStatusStart("Checking Output Limits");
            if (CommandLine["limit"] != null)
            {
                _stopProcessingAt = int.Parse(CommandLine["limit"]);
                Logger.WriteStatusFinal(true);
            }
            else
            {
                Logger.WriteStatusFinal(false, "NONE", ConsoleColor.Yellow);
            }

            // Check if we should launch default browser with creators website.
            Logger.WriteStatusStart("Checking Should Open URL's");
            if (CommandLine["browser"] != null)
            {
                bool.TryParse(CommandLine["browser"], out _shouldLoadWebsiteURL);
                Logger.WriteStatusFinal(true);
            }
            else
            {
                _shouldLoadWebsiteURL = false;
                Logger.WriteStatusFinal(false, "NOPE", ConsoleColor.Yellow);
            }

            // Check for forge directory.
            Logger.WriteStatusStart("Forge Directory");
            ForgePath = Path.Combine(StartupPath, "forge");
            Logger.WriteStatusFinal(Directory.Exists(ForgePath));
            if (ForgePath == null) return;

            // Check for mods directory.
            Logger.WriteStatusStart("Forge Mods Directory");
            ModsPath = Path.Combine(StartupPath, "mods");
            ForgeModPath = Path.Combine(ForgePath, "mods");
            Logger.WriteStatusFinal(Directory.Exists(ModsPath));
            if (ModsPath == null || ForgeModPath == null) return;

            // Check for control directory.
            Logger.WriteStatusStart("Control Directory");
            _ctrlPath = Path.Combine(StartupPath, "ctrl");
            Logger.WriteStatusFinal(Directory.Exists(_ctrlPath));
            if (_ctrlPath == null) return;

            if (!RunningInJenkins)
            {
                // Only parse JSON whe not running in Jenkins mode.
                ParseJSON(CommandLine);
            }

            // Check how we should parse our list.
            Logger.WriteStatusStart("Checking list sort method ");
            if (CommandLine["start"] != null)
            {
                if (CommandLine["start"].ToLower().Trim().Contains("top"))
                {
                    // Default is to start A -> Z.
                    _startFromTop = true;
                    Logger.WriteStatusFinal(true);
                }
                else if (CommandLine["start"].ToLower().Trim().Contains("bottom"))
                {
                    _startFromTop = false;
                    Logger.WriteStatusFinal(true);
                }
                else
                {
                    // Use the default.
                    _startFromTop = true;
                    Logger.WriteStatusFinal(false);
                }
            }
            else
            {
                // Switch not found!
                _startFromTop = true;
                Logger.WriteStatusFinal(false);
            }

            // Clean forge testing environment completely even config directory.
            Forge.CleanForgeDirectory(true, true);

            // Populate our control mod instance.
            if (PopulateControlMod()) return;

            if (!RunningInJenkins)
            {
                // Only perform this check when not running in Jenkins.
                CheckEmptyFolders();
            }

            // Display information about what the various flags actually mean.
            Logger.WriteLine("[START MAIN LOOP]");
            
            // Print out info for mono process on Linux.
            if (RunningInJenkins)
            {
                Console.WriteLine("JVM: " + javaExecutable);
                Console.WriteLine("ARGS: " + javaArguments);
                Console.WriteLine("FORGE: " + ForgePath);
            }

            // Run our control mod by itself to ensure it's config pre-exists.
            Logger.WriteStatusStart("(*)" + CtrlModDatas.Name + " [" + CtrlModDatas.Version + "]");
            LaunchForge(CtrlModDatas, true);

            // Prevent null exception when using Jenkins test.
            if (ModDatas == null)
            {
                ModDatas = new Dictionary<string, ModData>();
            }

            // Start main loop to test mod compatibility.
            int totalModsProcessed = 0;
            foreach (var mod in ModDatas)
            {
                // Check to see if we have gone over some predetermined limit.
                if (_stopProcessingAt > 0 && _stopProcessingAt <= ModDatas.Count() &&
                    _stopProcessingAt < totalModsProcessed)
                {
                    Logger.WriteLine("Stopping test at " + _stopProcessingAt + " results!");
                    break;
                }

                // Print the first part of this mod starting processing.
                totalModsProcessed++;
                if (!string.IsNullOrEmpty(mod.Value.Version))
                {
                    Logger.WriteStatusStart("(" + totalModsProcessed + "/" + ModDatas.Count() + ")" +
                                            mod.Value.Name.Trim() +
                                            " [" +
                                            mod.Value.Version.Trim() + "]");
                }
                else
                {
                    Logger.WriteStatusStart("(" + totalModsProcessed + "/" + ModDatas.Count() + ")" + mod.Value.Name);
                }

                // Clean out any files from previous run.
                Forge.CleanForgeDirectory(false, false);

                // Remove same amount of points required for deps.
                mod.Value.ModScore -= mod.Value.Dependencies.Count();

                // Start forge java process and event monitor for folder creation.
                LaunchForge(mod.Value, false);

                // Mod flag should be empty.
                if (!RunningInJenkins && string.IsNullOrEmpty(mod.Value.ModFlag) || mod.Value.RealFail 
                    /* || mod.Value.ErrorsOnStartup || mod.Value.ClientOnly */)
                {
                    // Write out a fail.txt because we know why this mod failed.
                    string failPath = Path.Combine(ModsPath, mod.Value.Name, "fail.txt");
                    using (var writer = new StreamWriter(failPath))
                    {
                        writer.WriteLine(mod.Value.Name);
                        writer.WriteLine(mod.Value.Version);
                        writer.WriteLine(mod.Value.CrashReport);
                        writer.Flush();
                    }
                }

                // Load site for offending mod that needs more info on Need More Mods.
                if (!RunningInJenkins && _shouldLoadWebsiteURL && !mod.Value.TestResult &&
                    !mod.Value.RealFail && !string.IsNullOrEmpty(mod.Value.ModFlag) &&
                    !string.IsNullOrEmpty(mod.Value.URL) && !_launchedURLs.Contains(mod.Value.URL))
                {
                    // We only want to know when mods are missing not when there are duplicates.
                    if (mod.Value.ModFlag == "MISSING")
                    {
                        Process.Start(mod.Value.URL);
                        _launchedURLs.Add(mod.Value.URL);
                    }
                }

                // Decrease the score to one for failing the test.
                if (mod.Value.IDConflict && !mod.Value.TestResult)
                {
                    mod.Value.ModScore = 1;
                }

                // Check score minimum.
                if (mod.Value.ModScore < 1)
                {
                    mod.Value.ModScore = 1;
                }

                // Check score maximum.
                if (mod.Value.ModScore > 5)
                {
                    mod.Value.ModScore = 5;
                }

                // Create formatted HTML page with all gathered data.
                mod.Value.HTML = CreateHTMLResults(mod.Value);
            }

            // Main loop ended, print process results.
            Logger.WriteLine("[FINISHED LOOP]");
            Forge.CleanForgeDirectory(true, true);

            if (!RunningInJenkins)
            {
                // Create CSV file containing all test results.
                Logger.WriteStatusStart("Create XML Result");
                CreateWXRFile();
                Logger.WriteStatusFinal(true);

                // Quit the program when user presses any key.
                Logger.WriteLine("Finshed Testing! Press any key to quit...");
                Console.ReadLine();
            }

            // Goodbye!
            Environment.Exit(0);
        }

        private static void CheckEmptyFolders()
        {
            // Check if any of the mod folders are empty and prevent execution if that is the case.
            Logger.WriteLine("[CHECK FOR EMPTY MOD FOLDERS]");

            SearchMods:
            // Initial mod parsing pass.
            PopulateModList();

            bool foldersEmpty = false;

            if (_startFromTop)
            {
                foreach (var modData in ModDatas)
                {
                    if (!modData.Value.Filenames.Any())
                    {
                        foldersEmpty = true;
                        if (string.IsNullOrEmpty(modData.Value.Name)) continue;
                        if (!string.IsNullOrEmpty(modData.Value.URL) && !RunningInJenkins)
                        {
                            Process.Start(modData.Value.URL);
                            Logger.WriteLine(modData.Value.Name);
                            Logger.WriteLine("To continue onto next empty folder type 'next' to continue...");
                            string confirmTesting = Console.ReadLine();
                            if (!string.IsNullOrEmpty(confirmTesting) &&
                                confirmTesting.ToLower().Trim().Contains("next"))
                            {
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (var modData in ModDatas.Reverse())
                {
                    if (!modData.Value.Filenames.Any())
                    {
                        foldersEmpty = true;
                        if (string.IsNullOrEmpty(modData.Value.Name)) continue;
                        if (!string.IsNullOrEmpty(modData.Value.URL) && !RunningInJenkins)
                        {
                            Process.Start(modData.Value.URL);
                            Logger.WriteLine(modData.Value.Name);
                            Logger.WriteLine("To continue onto next empty folder type 'next' to continue...");
                            string confirmTesting = Console.ReadLine();
                            if (!string.IsNullOrEmpty(confirmTesting) &&
                                confirmTesting.ToLower().Trim().Contains("next"))
                            {
                                break;
                            }
                        }
                    }
                }
            }

            if (foldersEmpty)
            {
                // Refresh mod internal information.
                goto SearchMods;
            }

            // Lets get this party started!
            if (!RunningInJenkins)
            {
                Logger.WriteLine("[CONFIRM MAIN LOOP]");
                Logger.WriteLine("To continue with testing type 'yes' to continue...");
                Logger.WriteLine("Warning: This will take a really long time with 100+ mods.");
                string confirmMainLoop = Console.ReadLine();
                if (!string.IsNullOrEmpty(confirmMainLoop) && confirmMainLoop.ToLower().Trim() != "yes")
                {
                    Logger.WriteLine("Testing aborted by user or you need magic typing wand!");
                    Logger.WriteLine("Press any key to quit the application...");
                    Console.ReadLine();
                    Environment.Exit(-1);
                }
            }
        }

        private static void ParseJSON(Arguments CommandLine)
        {
            // We are entering the next section of the program now!
            Console.WriteLine("[CHECK FOR JSON PARSING]");

            // Check if we should parse Not Enough Mods JSON.
            Logger.WriteStatusStart("Checking Not Enough Mods JSON");
            if (CommandLine["nem"] != null)
            {
                Logger.WriteStatusFinal(true);
                NEM.QueryNotEnoughMods(CommandLine);
            }
            else
            {
                Logger.WriteStatusFinal(false, "NOPE", ConsoleColor.Yellow);
            }

            // Check if we should parse MCF Mod list JSON.
            Logger.WriteStatusStart("Checking MCF Mod List JSON");
            if (CommandLine["mcf"] != null)
            {
                Logger.WriteStatusFinal(true);
                MCF.QueryMCFModList(CommandLine);
            }
            else
            {
                Logger.WriteStatusFinal(false, "NOPE", ConsoleColor.Yellow);
            }

            if (!RunningInJenkins)
            {
                // Wait before moving to next part.
                Console.WriteLine("[FINISHED JSON PARSING]");
                Console.WriteLine("Press any key to continue with empty folder test...");
                Console.ReadLine();
            }
        }

        private static string CreateHTMLResults(ModData modData)
        {
            // Creates formatted HTML of final page for site.
            var finalHTML = new StringBuilder();

            // Star rating on top of page below title and version (if avaliable).
            finalHTML.AppendLine("Compatibility Rating: [author-post-rating]");

            // Description of the mod and what it does.
            if (!string.IsNullOrEmpty(modData.Description))
            {
                finalHTML.AppendLine("<h2>Description</h2>");
                finalHTML.AppendLine("<p style=" + Util.Quotation + "text-align: justify" + Util.Quotation + ">" +
                                     modData.Description + "</p>");
            }

            // Comment from author regarding latest version.
            if (!string.IsNullOrEmpty(modData.Comment))
            {
                finalHTML.AppendLine("<h2>Notes</h2>");
                finalHTML.AppendLine("<p style=" + Util.Quotation + "text-align: justify" + Util.Quotation + ">" +
                                     modData.Comment.Replace("Requires Forge,", string.Empty) + "</p>");
            }

            // Author(s) information.
            if (!string.IsNullOrEmpty(modData.Authors))
            {
                finalHTML.AppendLine("<h2>Author(s)</h2>");
                finalHTML.AppendLine(modData.Authors);
            }

            // Supported versions of minecraft are listed and if not we hard code the most current.
            finalHTML.AppendLine("<h2>Supported Minecraft Versions</h2>");
            finalHTML.AppendLine("<ul>");
            if (modData.Supports != null && modData.Supports.Any())
            {
                foreach (string mcVersion in modData.Supports.Split(','))
                {
                    if (!string.IsNullOrEmpty(mcVersion))
                    {
                        finalHTML.AppendLine("<li>" + mcVersion + "</li>");
                    }
                }
            }
            else
            {
                finalHTML.AppendLine("<li>1.6.4</li>");
            }
            finalHTML.AppendLine("</ul>");

            // Print out any know problems with this mod and target one if applicable.
            finalHTML.AppendLine("<h2>Compatibility Issues</h2>");
            finalHTML.AppendLine("<ul>");

            // Tell if this is a client only mod or not.
            if (modData.ModFlag == "CLIENT" || modData.ClientOnly)
            {
                finalHTML.AppendLine("<li>Client-Only (crashes server)</li>");
            }

            if (modData.ModFlag == "SHAME" || modData.ErrorsOnStartup)
            {
                finalHTML.AppendLine("<li>Error(s) detected during mod initialization.</li>");
            }

            // Report ID conflict if there is a crash report.
            switch (modData.ModFlag)
            {
                case "SHAME":
                    {
                        if (!modData.TestResult && modData.ErrorsOnStartup)
                        {
                            finalHTML.AppendLine("<li>Started but with warnings!</li>");
                        }
                        else if (modData.TestResult && modData.ErrorsOnStartup)
                        {
                            finalHTML.AppendLine("<li>Mod(s) Crashed!</li>");
                        }
                    }
                    break;
                case "DUPE":
                    {
                        finalHTML.AppendLine("<li>Duplicate Mod(s)</li>");
                    }
                    break;
                case "MISSING":
                    {
                        finalHTML.AppendLine("<li>Missing Mod(s)</li>");
                    }
                    break;
                default:
                    {
                        // This is only true if the test failed.
                        if (modData.IDConflict)
                        {
                            finalHTML.AppendLine("<li>ID Conflict</li>");
                        }
                    }
                    break;
            }

            // Report everything is being fine!
            if (modData.TestResult && !modData.ErrorsOnStartup)
            {
                if (string.IsNullOrEmpty(modData.ModFlag))
                {
                    finalHTML.AppendLine("<li>No problems during tests!</li>");
                }
            }

            // Report dependancy mods for record keeping sake and understanding of point negatives.
            if (modData.Dependencies.Any())
            {
                foreach (object dependency in modData.Dependencies)
                {
                    if (!string.IsNullOrEmpty(dependency as string))
                    {
                        finalHTML.AppendLine("<li>Requires: " + dependency + "</li>");
                    }
                }
            }
            finalHTML.AppendLine("</ul>");

            // Exception information (if avaliable).
            if (!modData.TestResult && !string.IsNullOrEmpty(modData.CrashReport))
            {
                // Problem.
                finalHTML.AppendLine("<h2>Java Exception</h2>");
                finalHTML.AppendLine("<blockquote>" + Util.Truncate(modData.CrashReport, 500) + "</blockquote>");

                // Solution to ID conflict exception.
                finalHTML.AppendLine("<h2>Solution/Fix</h2>");

                switch (modData.ModFlag)
                {
                    case "SHAME":
                        {
                            finalHTML.AppendLine("<p style=" + Util.Quotation + "text-align: justify" + Util.Quotation +
                                                 ">Please contact the author(s) " + modData.Authors +
                                                 ", and tell them that their mod " + modData.Name +
                                                 " is crashing on startup with latest Minecraft Forge 1.6.4-9.11.1.965. Remember to be polite and ask if they could kindly fix it!</p>");
                        }
                        break;
                    case "CLIENT":
                        {
                            finalHTML.AppendLine("<p style=" + Util.Quotation + "text-align: justify" + Util.Quotation +
                                                 ">Our tests have shown this mod is a client-only mod which means that it is intended to only run on the Minecraft client and does not consume any Block or Item ID's. If you attempt to run this mod on the server it will cause it to crash because it is trying to locate class files and data that is not avaliable to the server and only the client.</p>");
                        }
                        break;
                    case "DUPE":
                        {
                            finalHTML.AppendLine("<p style=" + Util.Quotation + "text-align: justify" + Util.Quotation +
                                                 ">This is a generic stub and is here for the maintainer of this list to keep track of his own inability to make sure that he only has one copy of the mod in the folder before running the testing software that generates these pages! Tsk tsk!</p>");
                        }
                        break;
                    case "MISSING":
                        {
                            finalHTML.AppendLine("<p style=" + Util.Quotation + "text-align: justify" + Util.Quotation +
                                                 ">This mod attempted to run but requires another file, and until the maintainer of this list sorts that out I am going to just print this annoying message that taunts him.</p>");
                        }
                        break;
                    default:
                        {
                            finalHTML.AppendLine("<p style=" + Util.Quotation + "text-align: justify" + Util.Quotation +
                                                 ">Change the ID conflicts in " + modData.Name + " or in " +
                                                 CtrlModDatas.Name +
                                                 " to solve the conflict. If you have existing items in the world with either mod place preference on the one you had first and modify the other mod to use other ID's in that circumstance. " +
                                                 "If you do not want to manually edit your configuration files you can also use another mod to do it for you! Try out <a href=" +
                                                 Util.Quotation + "http://www.minecraftforum.net/topic/1928632-" +
                                                 Util.Quotation +
                                                 ">Idfix Minus</a> and it will do all the hard work for you!</p>");
                        }
                        break;
                }
            }

            // Print out external link to mod if any exists, also adding source code link below it if avaliable.
            if (!string.IsNullOrEmpty(modData.URL) &&
                string.IsNullOrEmpty(modData.SourceCodeLink))
            {
                finalHTML.AppendLine("<h2>External Link</h2>");
                finalHTML.AppendLine("<a href=" + Util.Quotation + modData.URL + Util.Quotation + ">" + modData.Name +
                                     " Site</a>");
            }
            else if (!string.IsNullOrEmpty(modData.URL) &&
                     !string.IsNullOrEmpty(modData.SourceCodeLink))
            {
                finalHTML.AppendLine("<h2>External Links</h2>");
                finalHTML.AppendLine("<a href=" + Util.Quotation + modData.URL + Util.Quotation + ">" + modData.Name +
                                     " Site</a>");
                finalHTML.AppendLine("<a href=" + Util.Quotation + modData.SourceCodeLink + Util.Quotation + ">" +
                                     modData.Name + " Sourcecode</a>");
            }

            return finalHTML.ToString();
        }

        private static void CreateWXRFile()
        {
            var wxr = new WordPressWXR();
            wxr.Init(ModDatas);

            // XML Namespace
            var nameSpaces = new XmlSerializerNamespaces();
            nameSpaces.Add("excerpt", "http://wordpress.org/export/1.2/excerpt/");
            nameSpaces.Add("content", "http://purl.org/rss/1.0/modules/content/");
            nameSpaces.Add("wfw", "http://wellformedweb.org/CommentAPI/");
            nameSpaces.Add("dc", "http://purl.org/dc/elements/1.1/");
            nameSpaces.Add("wp", "http://wordpress.org/export/1.2/");

            try
            {
                TextWriter writer =
                    new StreamWriter(Path.Combine(StartupPath,
                                                  CtrlModDatas.Name.Replace(" ", string.Empty) + "_" +
                                                  CtrlModDatas.Version + "(" + ModDatas.Count() + ")" + "_" +
                                                  DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xml"));

                var serializer = new XmlSerializer(typeof (WordPressWXR));
                serializer.Serialize(writer, wxr, nameSpaces);
                writer.Close();
            }
            catch (Exception)
            {
                // Nothing to see here, move along.
            }
        }

        private static void PopulateModList()
        {
            // Count number of mods we are going to execute in loop and save to list.
            try
            {
                if (ModDatas == null)
                {
                    ModDatas = new Dictionary<string, ModData>();
                }

                int modCount = 0;
                int emptyCount = 0;
                string[] _modDirectories = Directory.GetDirectories(ModsPath, "*.*", SearchOption.AllDirectories);

                foreach (string modDir in _modDirectories)
                {
                    modCount++;

                    string modFromPath = Path.GetFileName(modDir);
                    if (string.IsNullOrEmpty(modFromPath)) continue;

                    if (modCount < _modDirectories.Count())
                    {
                        Console.Write("\r" + "#" + modCount + " - {0}                          ", modFromPath);
                    }
                    else
                    {
                        Console.Write("\r" +
                                      "                                                                          ");
                        if (emptyCount > 0)
                        {
                            Logger.WriteLine("\r" + "Prepared " + modCount + " mods for testing! Detected " +
                                             emptyCount + " empty mod folders.");
                        }
                        else
                        {
                            Logger.WriteLine("\r" + "Prepared " + modCount + " mods for testing!");
                        }
                    }

                    string[] _modFiles =
                        Directory.GetFiles(modDir, "*.*", SearchOption.TopDirectoryOnly)
                                 .Where(f => _modExtensions.Contains(f.Split('.').Last().ToLower()))
                                 .ToArray();

                    if (ModDatas.ContainsKey(modFromPath))
                    {
                        ModDatas[modFromPath].Filenames = _modFiles;
                        if (ModDatas[modFromPath].Filenames.Any())
                        {
                            ModDatas[modFromPath].Ready = true;
                        }
                        else
                        {
                            emptyCount++;
                        }
                    }
                    else
                    {
                        ModDatas.Add(modFromPath, new ModData(modFromPath, "[LATEST]", _modFiles));
                    }
                }
            }
            catch (Exception err)
            {
                // Display error and wait for input from user.
                Logger.WriteLine(err.Message);
                Console.ReadLine();
                Environment.Exit(-1);
            }
        }

        private static bool PopulateControlMod()
        {
            // Populate our control mod instance.
            Logger.WriteStatusStart("Create Control Mod");
            try
            {
                string[] ctrlFiles =
                    Directory.GetFiles(_ctrlPath, "*.*", SearchOption.TopDirectoryOnly)
                             .Where(f => _modExtensions.Contains(f.Split('.').Last().ToLower()))
                             .ToArray();
                CtrlModDatas.Filenames = ctrlFiles;
                Logger.WriteStatusFinal(true);
            }
            catch (Exception)
            {
                Logger.WriteStatusFinal(false);
                Console.ReadLine();
                Environment.Exit(-1);
            }
            return false;
        }

        private static void LaunchForge(ModData mod, bool ctrlMod)
        {
            // Keeps track if anything went wrong for reporting purposes.
            bool wasErr = false;

            // We have run the simulation with this mod.
            mod.TestRun = true;

            if (!RunningInJenkins)
            {
                // Check inside the mods directory for pass.txt and skip if needed to save time.
                string passPath = Path.Combine(ModsPath, mod.Name, "pass.txt");
                if (File.Exists(passPath) && !ctrlMod)
                {
                    // We have run the simulation with this mod.
                    mod.TestResult = true;
                    mod.CrashReport = string.Empty;
                    Logger.WriteStatusFinal(true);
                    return;
                }

                // Check for fail.txt so we don't have to keep testing mods we know are not good.
                string failPath = Path.Combine(ModsPath, mod.Name, "fail.txt");
                if (File.Exists(failPath) && !ctrlMod)
                {
                    // We have run the simulation with this mod.
                    mod.TestResult = false;
                    Logger.WriteStatusFinal(false);
                    return;
                }
            }

            // Check for mod dependencies and copy them into the folder.
            if (mod.Dependencies != null && mod.Dependencies.Any())
            {
                foreach (object modDep in mod.Dependencies)
                {
                    if (string.IsNullOrEmpty(modDep.ToString())) continue;
                    if (ModDatas.ContainsKey(modDep.ToString()))
                    {
                        Forge.CopyModFiles(ModDatas[modDep.ToString()]);
                    }
                }
            }

            Forge.CopyModFiles(mod);

            // Launch Minecraft Forge Java Process...
            using (var _forgeProcess = new Process())
            {
                _forgeProcess.StartInfo.FileName = javaExecutable;
                _forgeProcess.StartInfo.Arguments = javaArguments;
                _forgeProcess.StartInfo.WorkingDirectory = ForgePath;
                _forgeProcess.StartInfo.UseShellExecute = false;
                _forgeProcess.StartInfo.RedirectStandardOutput = true;
                _forgeProcess.StartInfo.RedirectStandardError = true;

                var output = new StringBuilder();
                var error = new StringBuilder();

                using (var outputWaitHandle = new AutoResetEvent(false))
                using (var errorWaitHandle = new AutoResetEvent(false))
                {
                    _forgeProcess.OutputDataReceived += (sender, e) =>
                        {
                            if (e.Data == null)
                            {
                                try
                                {
                                    // ReSharper disable AccessToDisposedClosure
                                    outputWaitHandle.Set();
                                    // ReSharper restore AccessToDisposedClosure
                                }
                                catch (Exception)
                                {
                                    wasErr = true;
                                    if (RunningInJenkins)
                                    {
                                        Environment.Exit(-1);
                                    }
                                }
                            }
                            else
                            {
                                output.AppendLine(e.Data);
                                if (DetectForgeCrash(e.Data, mod))
                                {
                                    wasErr = true;

                                    try
                                    {
                                        // ReSharper disable AccessToDisposedClosure
                                        _forgeProcess.Kill();
                                        // ReSharper restore AccessToDisposedClosure
                                    }
                                    catch (Exception)
                                    {
                                        // Nothing to see here.
                                        if (RunningInJenkins)
                                        {
                                            Environment.Exit(-1);
                                        }
                                    }
                                }
                            }
                        };

                    _forgeProcess.ErrorDataReceived += (sender, e) =>
                        {
                            if (e.Data == null)
                            {
                                try
                                {
                                    // ReSharper disable AccessToDisposedClosure
                                    errorWaitHandle.Set();
                                    // ReSharper restore AccessToDisposedClosure
                                }
                                catch (Exception)
                                {
                                    wasErr = true;
                                    if (RunningInJenkins)
                                    {
                                        Environment.Exit(-1);
                                    }
                                }
                            }
                            else
                            {
                                error.AppendLine(e.Data);
                                if (DetectForgeCrash(e.Data, mod))
                                {
                                    wasErr = true;
                                    try
                                    {
                                        // ReSharper disable AccessToDisposedClosure
                                        _forgeProcess.Kill();
                                        // ReSharper restore AccessToDisposedClosure
                                    }
                                    catch (Exception)
                                    {
                                        // Nothing to see here.
                                        if (RunningInJenkins)
                                        {
                                            Environment.Exit(-1);
                                        }
                                    }
                                }
                            }
                        };

                    // Start Minecraft Forge with the arguments we specified.
                    _forgeProcess.Start();

                    _forgeProcess.BeginOutputReadLine();
                    _forgeProcess.BeginErrorReadLine();

                    if (_forgeProcess.WaitForExit(timeout) &&
                        outputWaitHandle.WaitOne(timeout))
                    {
                        // Process completed. Check process.ExitCode here.
                        wasErr = false;
                    }
                    else
                    {
                        try
                        {
                            _forgeProcess.Kill();
                        }
                        catch (Exception)
                        {
                            // Nothing to see here.
                            if (RunningInJenkins)
                            {
                                Environment.Exit(-1);
                            }
                        }

                        // Timed out.
                        wasErr = true;
                    }
                }
            }

            // Delete the mod files that we used during our test, only do this if it's not control though.
            if (!ctrlMod)
            {
                // Removes forge mods folder and recreates directory and copies over control mod.
                Directory.Delete(ForgeModPath, true);
                Directory.CreateDirectory(ForgeModPath);
                Forge.CopyModFiles(CtrlModDatas);
            }

            // Write out the final status for this mod.
            if (mod.TestResult && !wasErr && !mod.ClientOnly &&
                !mod.ErrorsOnStartup && !mod.DuplicateFileDetected)
            {
                //// Write out a pass.txt so we don't have to test this mod again!
                //if (!ctrlMod)
                //{
                //    using (var writer = new StreamWriter(passPath))
                //    {
                //        writer.WriteLine(mod.Name);
                //        writer.WriteLine(mod.Version);
                //        writer.Flush();
                //    }
                //}

                Logger.WriteStatusFinal(true);
            }
            else if (mod.DuplicateFileDetected)
            {
                // Duplicate mod detected in the mods folder, silly developer...
                Logger.WriteStatusFinal(true, "DUPE", ConsoleColor.Cyan);
            }
            else if (mod.ClientOnly)
            {
                // Mod threw errors but did not actually crash.
                Logger.WriteStatusFinal(true, "CLIENT", ConsoleColor.Blue);
            }
            else if (mod.ErrorsOnStartup)
            {
                // Mod threw errors but did not actually crash.
                Logger.WriteStatusFinal(true, "WARN", ConsoleColor.Yellow);
            }
            else if (mod.RealFail && !mod.TestResult)
            {
                // Registered as actual ID conflict and FML failed to start.
                Logger.WriteStatusFinal(true, "IDFIX", ConsoleColor.Magenta);
            }
            else if (!mod.TestResult && !mod.RealFail)
            {
                // Error occured while testing the mod.
                if (wasErr)
                {
                    Logger.WriteStatusFinal(false, "FAIL", ConsoleColor.DarkRed);
                }
                else
                {
                    Logger.WriteStatusFinal(false);
                }

                if (RunningInJenkins)
                {
                    Environment.Exit(-1);
                }
            }
        }

        private static bool DetectForgeCrash(string input, ModData mod)
        {
            // Forge has started up without error!
            if (input.Contains("[Minecraft-Server] Done"))
            {
                mod.TestResult = true;
                return true;
            }

            // Missing mod exception requres intervention by developer to fix dependency issues.
            if (input.Contains("cpw.mods.fml.common.MissingModsException"))
            {
                // Only write the !M! if we have no existing flag.
                if (!mod.MissingMods)
                {
                    mod.MissingMods = true;
                }
                mod.ModFlag = "MISSING";
                mod.TestResult = false;
                mod.RealFail = false;
                mod.CrashReport = string.Empty;
                return true;
            }

            // Mod requires lines are important to have, rip them out of the incoming text.
            if (input.Contains("[SEVERE] [ForgeModLoader] The mod ") && input.Contains(" requires mods "))
            {
                // Only write the !M! if we have no existing flag.
                if (!mod.MissingMods)
                {
                    mod.MissingMods = true;
                }
                Logger.WriteStatusFinal(false);
                Logger.WriteLine(input);
                mod.ModFlag = "MISSING";
                mod.TestResult = false;
                mod.RealFail = false;
                mod.CrashReport = string.Empty;
                return true;
            }

            // Duplicate mod exception requires a pause on ops for a moment.
            if (input.Contains("cpw.mods.fml.common.DuplicateModsFoundException"))
            {
                // Only write the !D! if we have no existing flag.
                if (!mod.DuplicateFileDetected)
                {
                    mod.DuplicateFileDetected = true;
                }
                Logger.WriteStatusFinal(false);
                mod.ModFlag = "DUPE";
                mod.TestResult = false;
                mod.RealFail = false;
                mod.CrashReport = string.Empty;
                return true;
            }

            // Subtract points for terrible coders doing terrible things.
            if (input.Contains("java.lang.ClassNotFoundException") ||
                input.Contains("java.lang.VerifyError: Bad type on operand stack") ||
                input.Contains("java.lang.NoSuchMethodError") ||
                input.Contains("java.lang.ArrayIndexOutOfBoundsException") ||
                input.Contains("java.lang.NullPointerException") ||
                input.Contains("java.lang.NoSuchFieldError") ||
                input.Contains("java.lang.AbstractMethodError"))
            {
                // You lose two points for being a terrible coder.
                mod.ModFlag = "SHAME";

                // Set a flag in the mod file that makes us remember errors occured.
                if (!mod.ErrorsOnStartup)
                {
                    mod.ErrorsOnStartup = true;
                }

                // Ensures that fail.txt is written.
                mod.TestResult = false;
                mod.CrashReport = string.Empty;
                mod.RealFail = true;
                return true;
            }

            // Client only mods do not affect the test.
            if (input.Contains("java.lang.NoClassDefFoundError: net/minecraft/client/renderer/EntityRenderer") ||
                input.Contains("java.lang.NoClassDefFoundError: net/minecraft/client/gui/GuiScreen") ||
                input.Contains("java.lang.NoClassDefFoundError: net/minecraft/client/multiplayer/WorldClient") ||
                input.Contains(
                    "cpw.mods.fml.common.LoaderException: java.lang.NoClassDefFoundError: net/minecraft/client/entity/EntityClientPlayerMP") ||
                input.Contains(
                    "cpw.mods.fml.common.LoaderException: java.lang.NoClassDefFoundError: net/minecraft/client/Minecraft") ||
                input.Contains("java.lang.NoClassDefFoundError: Lnet/minecraft/client/Minecraft") ||
                input.Contains(
                    "cpw.mods.fml.common.LoaderException: java.lang.NoClassDefFoundError: net/minecraft/client/gui/ScaledResolution") ||
                input.Contains(
                    "java.lang.NoSuchMethodError: net.minecraft.src.ModLoader.getMinecraftInstance()Lnet/minecraft/client/Minecraft") ||
                input.Contains("java.lang.NoClassDefFoundError: net/minecraft/client/entity/EntityClientPlayerMP") ||
                input.Contains(
                    "cpw.mods.fml.common.LoaderException: java.lang.NoClassDefFoundError: cpw/mods/fml/client/GuiModList") ||
                input.Contains("java.lang.NoClassDefFoundError: net/minecraft/client/settings/KeyBinding") ||
                input.Contains(
                    "java.lang.NoClassDefFoundError: net/minecraft/client/renderer/tileentity/TileEntitySpecialRenderer") ||
                input.Contains("java.lang.NoClassDefFoundError: [Lnet/minecraft/client/settings/KeyBinding"))
            {
                // Only write the !C! if we have no existing flag.
                if (!mod.ClientOnly)
                {
                    mod.ClientOnly = true;
                }

                mod.ModFlag = "CLIENT";
                mod.TestResult = true;
                mod.CrashReport = string.Empty;

                // Ensures that fail.txt is written.
                mod.RealFail = true;

                return true;
            }

            // Forge has reported some custom ID confliction exception.
            if (input.Contains("There was an ID conflict") ||
                input.Contains("already occupied") ||
                input.ToLower().Contains("slot occupied"))
            {
                // Mark the mod as failing because of actual problems we want to detect.
                mod.RealFail = true;
                mod.TestResult = false;
                mod.IDConflict = true;
                mod.CrashReport = string.Empty;
            }

            // Forge has crashed (imagine that).
            if (input.Contains("crash-reports"))
            {
                // Collect crash report data if forge crashed.
                string crashPath = Path.Combine(ForgePath, "crash-reports");
                if (Directory.Exists(crashPath))
                {
                    string[] crashLog = Directory.GetFiles(crashPath, "*.txt");
                    if (crashLog.Count() > 1)
                    {
                        Logger.WriteLine("Multiple error logs detected in crash report folder.\nThis is bad!");
                    }
                    else
                    {
                        mod.CrashReport = File.ReadAllText(crashLog[0]);
                    }
                }

                // Check if you should lose another point for being a terrible programmer.
                if (mod.ModFlag == "SHAME" || mod.ModFlag == "CLIENT" || mod.ModFlag == "MISSING")
                {
                    mod.ModScore--;
                }

                // Mark the mod as failing because of actual problems we want to detect.
                mod.RealFail = true;
                mod.TestResult = false;
                return mod.IDConflict;
            }

            return false;
        }
    }
}