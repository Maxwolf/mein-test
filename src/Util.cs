﻿using System.Linq;
using System.Text;
using System.Xml;

namespace MeinTest
{
    public static class Util
    {
        public const string Quotation = "\"";

        public static XmlCDataSection StringToCData(string data)
        {
            //Thanks to lynxbleu for this workaround: http://social.msdn.microsoft.com/Forums/en-US/asmxandxml/thread/ee4562d8-dccf-4eed-8c33-2e8fb6a0b34c/
            // ReSharper disable CSharpWarnings::CS0612
            var doc = new XmlDataDocument();
            // ReSharper restore CSharpWarnings::CS0612
            XmlCDataSection cd = doc.CreateCDataSection(data);
            return cd;
        }

        /// <summary>
        ///     Get a substring of the first N characters.
        /// </summary>
        public static string Truncate(string source, int length)
        {
            if (source.Length > length)
            {
                source = source.Substring(0, length);
            }
            return source;
        }

        public static string RemoveSpecialCharacters(string str)
        {
            var sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }
    }
}