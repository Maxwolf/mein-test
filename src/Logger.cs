﻿using System;
using System.IO;
using System.Threading;

namespace MeinTest
{
    public class Logger
    {
        public static void WriteStatusStart(string title)
        {
            // Prints out a status line for whatever we want.
            string theText = (title + ":").PadRight(55, '.') + ("[").PadLeft(5, '.');
            Console.Write(theText);
            WriteToLogDirectly(theText, false);
        }

        public static void WriteStatusFinal(bool succeded)
        {
            WriteStatusFinal(succeded, string.Empty, ConsoleColor.Magenta);
        }

        public static void WriteStatusFinal(bool succeded, string outputText, ConsoleColor whatColor)
        {
            if (string.IsNullOrEmpty(outputText))
            {
                // Determine based on input if this is green or red text.
                if (succeded)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("OK");
                    WriteToLogDirectly("OK]", true);
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("FAIL");
                    WriteToLogDirectly("FAIL]", true);
                }
            }
            else
            {
                Console.ForegroundColor = whatColor;
                Console.Write(outputText.ToUpper().Trim());
                WriteToLogDirectly(outputText.ToUpper().Trim() + "]", true);
            }

            // Reset all the colors back to their defaults.
            Console.ResetColor();

            // Finish off the current line with ending bracket.
            Console.WriteLine("]");
            Thread.Sleep(42);
        }

        public static void WriteLine(string info)
        {
            // Write info to file on the disk.
            WriteToLogDirectly(info, true);

            // Write info to the console itself.
            Console.WriteLine(info);
        }

        public static void WriteToLogDirectly(string info, bool newLine)
        {
            // Only if allowed.
            if (Program.RunningInJenkins)
            {
                return;
            }

            try
            {
                if (newLine)
                {
                    File.AppendAllText(Path.Combine(Program.StartupPath, Program.StartTimestamp + ".log"),
                                       info + Environment.NewLine);
                }
                else
                {
                    File.AppendAllText(Path.Combine(Program.StartupPath, Program.StartTimestamp + ".log"), info);
                }
            }
            catch (Exception)
            {
                // Nothing to see here.
            }
        }
    }
}