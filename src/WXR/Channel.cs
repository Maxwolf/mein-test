﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MeinTest
{
    public class Channel
    {
        [XmlIgnore] private List<WordPressWXRItem> items = new List<WordPressWXRItem>();

        [XmlElement(Namespace = "http://wordpress.org/export/1.2/")] public string wxr_version = "1.2";

        [XmlElement("item")]
        public List<WordPressWXRItem> Items
        {
            get { return items; }
            set { items = value; }
        }

        public bool Init(Dictionary<string, ModData> currentMod)
        {
            try
            {
                foreach (var p in currentMod)
                {
                    // Only process mods that have been tested.
                    if (!p.Value.Ready) continue;
                    if (!p.Value.TestRun) continue;

                    var item = new WordPressWXRItem();
                    item.InitPost(p);
                    Items.Add(item);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}