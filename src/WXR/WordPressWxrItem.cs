﻿using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using System.Xml.Serialization;

namespace MeinTest
{
    public class WordPressWXRItem
    {
        [XmlIgnore] private const string author = "ronwolf";

        [XmlIgnore]
        private KeyValuePair<string, ModData> currentMod { get; set; }

        [XmlElement("title")]
        public XmlCDataSection title
        {
            get
            {
                // Don't print the version number if we don't have a nice one.
                if (string.IsNullOrEmpty(currentMod.Value.Version)
                    || currentMod.Value.Version == "[LATEST]")
                {
                    return Util.StringToCData(currentMod.Value.Name);
                }

                return Util.StringToCData(currentMod.Value.Name + " [" + currentMod.Value.Version + "]");
            }
            // ReSharper disable ValueParameterNotUsed
            set { }
            // ReSharper restore ValueParameterNotUsed
        }

        [XmlElement(Namespace = "http://purl.org/dc/elements/1.1/")]
        public XmlCDataSection creator
        {
            get { return Util.StringToCData(author); }
            // ReSharper disable ValueParameterNotUsed
            set { }
            // ReSharper restore ValueParameterNotUsed
        }

        [XmlElement("category")]
        public List<CategoriesAndTags> category
        {
            get
            {
                var l = new List<CategoriesAndTags>();

                // Category definition.
                var modCatatgory = new CategoriesAndTags {domain = "category", nicename = "moddb", text = "Mods"};
                l.Add(modCatatgory);

                // Post tags definition.
                // ReSharper disable LoopCanBeConvertedToQuery
                foreach (string alias in currentMod.Value.Aliases)
                {
                    if (string.IsNullOrEmpty(alias)) continue;
                    var modTag = new CategoriesAndTags
                        {
                            domain = "post_tag",
                            nicename = alias.Trim().ToLower(),
                            text = alias.Trim().ToUpperInvariant()
                        };
                    l.Add(modTag);
                }
                // ReSharper restore LoopCanBeConvertedToQuery

                return l;
            }
            // ReSharper disable ValueParameterNotUsed
            set { }
            // ReSharper restore ValueParameterNotUsed
        }

        [XmlElement(Namespace = "http://wordpress.org/export/1.2/")]
        public string comment_status
        {
            get { return "closed"; }
            // ReSharper disable ValueParameterNotUsed
            set { }
            // ReSharper restore ValueParameterNotUsed
        }

        [XmlElement(Namespace = "http://wordpress.org/export/1.2/")]
        public string ping_status
        {
            get { return "closed"; }
            // ReSharper disable ValueParameterNotUsed  
            set { }
            // ReSharper restore ValueParameterNotUsed
        }

        [XmlElement(Namespace = "http://wordpress.org/export/1.2/")]
        public string post_name
        {
            get { return currentMod.Value.Name.ToLower().Trim(); }
            // ReSharper disable ValueParameterNotUsed
            set { }
            // ReSharper restore ValueParameterNotUsed
        }

        [XmlElement(Namespace = "http://purl.org/rss/1.0/modules/content/")]
        public XmlCDataSection encoded
        {
            get { return Util.StringToCData(currentMod.Value.HTML); }
            // ReSharper disable ValueParameterNotUsed
            set { }
            // ReSharper restore ValueParameterNotUsed
        }

        [XmlElement(Namespace = "http://wordpress.org/export/1.2/")]
        public string status
        {
            get { return "publish"; }
            // ReSharper disable ValueParameterNotUsed
            set { }
            // ReSharper restore ValueParameterNotUsed
        }

        [XmlElement(Namespace = "http://wordpress.org/export/1.2/")]
        public string post_type
        {
            get { return "post"; }
            // ReSharper disable ValueParameterNotUsed
            set { }
            // ReSharper restore ValueParameterNotUsed
        }

        [XmlElement(Namespace = "http://wordpress.org/export/1.2/")]
        public List<PostMeta> postmeta
        {
            get
            {
                var lpm = new List<PostMeta>();
                var pm1 = new PostMeta {meta_key = "iconfile", meta_value = Util.StringToCData("processing.png")};

                var pm2 = new PostMeta
                    {
                        meta_key = "_pn_apr_rating",
                        meta_value =
                            Util.StringToCData(currentMod.Value.ModScore.ToString(CultureInfo.InvariantCulture))
                    };

                lpm.Add(pm1);
                lpm.Add(pm2);

                return lpm;
            }
            // ReSharper disable ValueParameterNotUsed
            set { }
            // ReSharper restore ValueParameterNotUsed
        }

        public void InitPost(KeyValuePair<string, ModData> minecraftMod)
        {
            currentMod = minecraftMod;
        }
    }
}