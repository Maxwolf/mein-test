﻿using System.Xml.Serialization;

namespace MeinTest
{
    public class CategoriesAndTags
    {
        [XmlAttribute("domain")] public string domain;

        [XmlAttribute("nicename")] public string nicename;

        [XmlText] public string text;
    }
}