﻿using System.Xml.Serialization;

namespace MeinTest
{
    public class Permalink
    {
        [XmlAttribute("isPermalink")] public bool isPermalink;

        [XmlText] public string text;
    }
}