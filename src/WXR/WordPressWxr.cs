﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace MeinTest
{
    [XmlRoot("rss")]
    public class WordPressWXR
    {
        [XmlIgnore] private readonly List<Channel> channelItems = new List<Channel>();

        [XmlElement("channel")]
        public List<Channel> ChannelItems
        {
            get { return channelItems; }
        }

        public void Init(Dictionary<string, ModData> mcMod)
        {
            var myChannel = new Channel();
            if (myChannel.Init(mcMod))
            {
                ChannelItems.Add(myChannel);
            }
        }
    }
}