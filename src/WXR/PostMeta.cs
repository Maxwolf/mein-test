﻿using System.Xml;
using System.Xml.Serialization;

namespace MeinTest
{
    public class PostMeta
    {
        [XmlElement(Namespace = "http://wordpress.org/export/1.2/")] public string meta_key;

        [XmlElement(Namespace = "http://wordpress.org/export/1.2/")] public XmlCDataSection meta_value;
    }
}