﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MeinTest
{
    public class Forge
    {
        public static void CopyModFiles(ModData mod)
        {
            // Copy the mods contents over to the forge directory.
            foreach (string modFile in mod.Filenames)
            {
                try
                {
                    if (!File.Exists(modFile)) continue;
                    // ReSharper disable AssignNullToNotNullAttribute
                    File.Copy(modFile, Path.Combine(Program.ForgeModPath, Path.GetFileName(modFile)));
                    // ReSharper restore AssignNullToNotNullAttribute
                }
                catch (Exception)
                {
                    // Nothing to see here.
                }
            }
        }

        public static void CleanForgeDirectory(bool wipeConfigs, bool wipeForgeMods)
        {
            // Create a list of paths we want to destroy.
            var filesDestroyed = new List<string>
                {
                    Path.Combine(Program.ForgePath, "ForgeModLoader-server-0.log.lck"),
                    Path.Combine(Program.ForgePath, "ForgeModLoader-server-1.log"),
                    Path.Combine(Program.ForgePath, "server.log"),
                    Path.Combine(Program.ForgePath, "server.log.lck"),
                    Path.Combine(Program.ForgePath, "ForgeModLoader-server-0.log"),
                    Path.Combine(Program.ForgePath, "idfix.txt"),
                };

            // Add more files ranges to the files to be destroyed.
            filesDestroyed.AddRange(Directory.GetFiles(Program.ForgePath, "*.log"));
            filesDestroyed.AddRange(Directory.GetFiles(Program.ForgePath, "*.lck"));
            filesDestroyed.AddRange(Directory.GetFiles(Program.ForgePath, "*.0"));
            filesDestroyed.AddRange(Directory.GetFiles(Program.ForgePath, "*.1"));
            filesDestroyed.AddRange(Directory.GetFiles(Program.ForgePath, "*.2"));
            filesDestroyed.AddRange(Directory.GetFiles(Program.ForgePath, "*.rtf"));
            filesDestroyed.AddRange(Directory.GetFiles(Program.ForgePath, "*.log"));

            // Create a list of directories that we want destroyed.
            var dirsDestroyed = new List<string>
                {
                    Path.Combine(Program.ForgePath, "crash-reports"),
                    Path.Combine(Program.ForgePath, "customnpcs"),
                    Path.Combine(Program.ForgePath, "database"),
                    Path.Combine(Program.ForgePath, "dynmap"),
                    Path.Combine(Program.ForgePath, "hats"),
                    Path.Combine(Program.ForgePath, "TickProfilerLogs")
                };

            // Destroy the needed files, checking for them before each one.
            foreach (string fmlFile in filesDestroyed)
            {
                if (File.Exists(fmlFile))
                {
                    try
                    {
                        File.Delete(fmlFile);
                    }
                    catch (Exception)
                    {
                        // Nothing to see here, move along.
                    }
                }
            }

            // Destroy the needed directories, checking for them before each one.
            foreach (string fmlDir in dirsDestroyed)
            {
                if (Directory.Exists(fmlDir))
                {
                    try
                    {
                        Directory.Delete(fmlDir, true);
                    }
                    catch (Exception)
                    {
                        // Nothing to see here, move along.
                    }
                }
            }

            // Only wipe configuration files if we are told to do so.
            if (wipeConfigs)
            {
                try
                {
                    Directory.Delete(Path.Combine(Program.ForgePath, "config"), true);
                }
                catch
                {
                }
            }

            // Only wipe forge mods if we are told to do so.
            if (wipeForgeMods)
            {
                try
                {
                    Directory.Delete(Program.ForgeModPath, true);
                }
                catch
                {
                }

                // Recreate the directory after making it since we need it as a target.
                Directory.CreateDirectory(Program.ForgeModPath);
            }
        }
    }
}