﻿using System;
using System.Collections.Generic;

namespace MeinTest
{
    public class ModData
    {
        public ModData(string name, string version, string[] filenames)
        {
            // Mod information.
            Name = name;
            Version = version;
            Filenames = filenames;
            Dependencies = new List<object>();

            // Test result cannot be entered on init.
            TestResult = false;

            // Cannot have a crash report on init.
            CrashReport = String.Empty;

            // All mods start with maximum number of points.
            ModScore = 5;
        }

        public ModData(string name, string version)
        {
            // Mod information.
            Name = name;
            Version = version;
            Filenames = new string[0];
            Dependencies = new List<object>();

            // Cannot have a crash report on init.
            CrashReport = String.Empty;

            // Test result cannot be entered on init.
            TestResult = false;

            // All mods start with maximum number of points.
            ModScore = 5;
        }

        public ModData()
        {
            Filenames = new string[0];
            Dependencies = new List<object>();

            // Test result cannot be entered on init.
            TestResult = false;

            // Cannot have a crash report on init.
            CrashReport = String.Empty;

            // All mods start with maximum number of points.
            ModScore = 5;
        }

        // Name of the mod.
        public string Name { get; set; }

        // Version of the mod.
        public string Version { get; set; }

        // External link to mod site.
        public string URL { get; set; }

        // Author(s) of the mod.
        public string Authors { get; set; }

        // Determines if mod is ready for testing or not.
        public bool Ready { get; set; }

        // Location of all mod related files.
        public string[] Filenames { get; set; }

        // Result of the compatibility test.
        public bool TestResult { get; set; }

        // Determines if we have actually been tested or not.
        public bool TestRun { get; set; }

        // Contents of the crash report.
        public string CrashReport { get; set; }

        // Final score for the mod.
        public int ModScore { get; set; }

        // List of strings that are mods this mod depends on.
        public List<object> Dependencies { get; set; }

        // Short piece of information about the mod.
        public string Comment { get; set; }

        // Stores the formatted version of the other data stored within this class.
        public string HTML { get; set; }

        // Determines if this is a real failure or one caused by lack of proper info about dependancies.
        public bool RealFail { get; set; }

        // Holds any special data pertaining to last inspection of the mod while running (Ex. missing mod, depedency problem).
        public string ModFlag { get; set; }

        // Full on description of the mod and what it's purpose is and possibly more info.
        public string Description { get; set; }

        // List of supported versions of Minecraft that this mod supports.
        public string Supports { get; set; }

        // Link to mods source code repository if it exists.
        public string SourceCodeLink { get; set; }

        // Determines if any common Java errors were detected during mod startup alongside the control mod.
        public bool ErrorsOnStartup { get; set; }

        // Stores value indicating if this is a client-only minecraft mod and therefor excluded from certain penalties.
        public bool ClientOnly { get; set; }

        // Stores value indicating if this mod has thrown a specific error saying it was unable to locate a needed mod to run.
        public bool MissingMods { get; set; }

        // Stores value indicating if FML has detected that it is trying to load the same mod twice.
        public bool DuplicateFileDetected { get; set; }

        // Stores the known shortnames for this mod which will be used as tags now.
        public string[] Aliases { get; set; }

        // Determines if there an ID conflict that was detected on this mod that we want to record crash-report from.
        public bool IDConflict { get; set; }
    }
}