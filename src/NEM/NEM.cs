﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MeinTest
{
    public class NEM
    {
        public static void QueryNotEnoughMods(Arguments CommandLine)
        {
            // Give me all that precious data!
            Logger.WriteStatusStart("Querying NotEnoughMods for JSON");
            List<NotEnoughMods> nemData;

            // Attempt to query JSON data from Not Enough Mods.
            try
            {
                nemData = JSON.DownloadJSONData<List<NotEnoughMods>>(CommandLine["nem"]);
            }
            catch (Exception)
            {
                Logger.WriteStatusFinal(false);
                return;
            }

            // Did something terrible happen?
            if (nemData == null)
            {
                Logger.WriteStatusFinal(false);
                return;
            }

            // No errors if we got this far!
            Logger.WriteStatusFinal(true);

            // Create the internal mod list if it has not already been made.
            if (Program.ModDatas == null)
            {
                Program.ModDatas = new Dictionary<string, ModData>();
            }

            // Now iterate through the mods and create folders based on their names.
            int totalMods = 0;
            foreach (NotEnoughMods item in nemData)
            {
                // Check for invalid version numbers.
                if (item.version.ToString() == "???" ||
                    item.version.ToString() == "dev-only" ||
                    item.version.ToString() == "-" ||
                    item.version.ToString() == "0" ||
                    item.version.ToString() == "1" ||
                    item.version.ToString() == "[]" ||
                    string.IsNullOrEmpty(item.version.ToString()) ||
                    string.IsNullOrWhiteSpace(item.version.ToString()))
                {
                    item.version = "LATEST";
                }

                totalMods++;
                if (totalMods < nemData.Count)
                {
                    Console.Write("\r" + "#" + totalMods + " - {0} [{1}]                          ", item.name,
                                  item.version);
                }
                else
                {
                    Console.Write("\r" + "                                                                          ");
                    Logger.WriteLine("\r" + "NEM: Deserialized " + totalMods + " mods!");
                }

                // Check if the mod about to be added is control mod and stop it.
                if (item.name == Program.CtrlModDatas.Name.Replace(" ", string.Empty))
                {
                    continue;
                }

                // Check if the mod about to be added is MinecraftForge and stop it.
                if (item.name.Replace(" ", string.Empty) == "MinecraftForge")
                {
                    continue;
                }

                string dirName = item.name;
                string dirPath = Path.Combine(Program.ModsPath, dirName);

                // Create mod directory if it does not exist.
                if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }

                // Save link to mod site in the folder for easy access.
                string urlPath = dirPath + Path.DirectorySeparatorChar + item.name + ".url";
                using (var writer = new StreamWriter(urlPath))
                {
                    writer.WriteLine("[InternetShortcut]");
                    writer.WriteLine("URL=" + item.longurl);
                    writer.Flush();
                }

                // Check if MinecraftForge is a dependancy and if so remove it.
                if (item.dependencies.Contains("MinecraftForge"))
                {
                    item.dependencies.Remove("MinecraftForge");
                }

                // Add mod to internal list for processing.
                var newMod = new ModData
                    {
                        Authors = item.author,
                        Name = item.name,
                        Comment = item.comment,
                        URL = item.longurl,
                        Version = item.version as string,
                        Dependencies = item.dependencies,
                        Aliases = item.aliases.Split(' ')
                    };

                Program.ModDatas.Add(item.name, newMod);
            }
        }
    }
}