﻿using System.Collections.Generic;

namespace MeinTest
{
    public class NotEnoughMods
    {
        public string name { get; set; }
        public object version { get; set; }
        public string longurl { get; set; }
        public string shorturl { get; set; }
        public string aliases { get; set; }
        public string comment { get; set; }
        public string modid { get; set; }
        public object dev { get; set; }
        public string author { get; set; }
        public int lastupdated { get; set; }
        public object prevversion { get; set; }
        public List<object> dependencies { get; set; }
    }
}