## Mein Test
Mein Test is a personal program written by Maxwolf to help him create a compatibility list on his site for Mad Science.
The program uses Forge 965 and is designed to work with Minecraft 1.6.4 (but can be upgraded for future releases).

### Installation

You will have to provide your own MinecraftServer binary .jar file and place it into the 'forge' folder for everything to work.

### Arguments

MeinTest.exe
Requires flags in order to properly execute, please use 'Launch_MeinTest.bat' for ease of use in launching. Remember to change it to use your mod name!

-name="MyAmazingMod"
Name of the control mod we are testing all other mods against.

-version=0.1
Version of the control mod we are testing all other mods against.

-nem=http://bot.notenoughmods.com/1.6.4.json
URL to parseable JSON for Not Enough Mods.

-mcf=http://modlist.mcf.li/api/v3/1.6.4.json
URL to parseable JSON for MCF Mod List.

-start=bottom
Determines if empty directory search should count from the top or bottom of the list, useful for having multiple people go on mod hunts to fill out 'mods' folder.

-browser=false
Determines if we should open default browser with mod page found in JSON to make downloading easier when there is an error (always occurs during empty folder test)

-jdk="C:\Program Files\Java\jdk1.7.0_45\bin\java.exe"
Tells MeinTest where the executable for your JVM lives so it can execute Forge.

-args="-jar forge-1.6.4-9.11.1.965-universal.jar -o true nogui"
Tell MeinTest the arguments to use while executing the JVM, you probably don't want to see the server GUI.

### Folders

ctrl
Contains control mod, or the mod that is going to be tested against every other mod and any dependencies it might have.

forge
Contains a copy of Minecraft Forge 9.11.1.965 which is executed by MeinTest during the main loop of the program.

mods
Contains a folder inside of it for every mod that we will test against. Inside mod folders you can find fail.txt or pass.txt depending on if test failed. Will also find URL link to site for mod, and the mod files themselves.

src
Contains all the C# source code for compiling and editing MeinTest. Written using Visual Studio 2012.

### WXR Creation

Mein Test after all testing has completed will create a XML file that is a valid WordPress WXR format for importing onto that site for mod comptability purposes.
It does not create the listing page, that is something that is left to the developer to implement in his own way.

The WXR files contain a page for every mod scanned and it's resulting score against the control (ctrl) mod.
It can then be uploaded to WordPress under Tools -> Import (make sure PHP execution time is set higher than normal to import 300+ pages as it can take about 1.5 minutes to import all pages).
 
### Developement
**Developer** - Maxwolf

### Contact
Join *#universalelectricity* on EsperNet (irc.esper.net:5555). Remember to follow the rules and respect others as with any other channel.

### License
The MIT License (MIT)

Copyright (c) 2014 Maxwolf Goodliffe

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
